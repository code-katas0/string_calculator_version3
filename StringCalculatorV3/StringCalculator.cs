﻿using System;
using System.Collections.Generic;

namespace StringCalculatorV3
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            var numberArray = get_Numbers(numbers);
            validate_Delimiters(numberArray);
            validate_Numbers(numberArray);
            int total = get_Total(numberArray);
            return total;
        }

        private List<string> get_Delimiters(string numbers)
        {
            var delimiterList = new List<string>();
            var delimiters = numbers.Substring(0, numbers.IndexOf("\n"));
            if (numbers.Contains("]"))
            {
                foreach(var delimiter in delimiters.Split('[', ']'))
                {
                    if(!string.IsNullOrEmpty(delimiter))
                    {
                        delimiterList.Add(delimiter);
                    }
                }
            }
            else
            {
                delimiterList.Add(delimiters);
            }

            return delimiterList;
        }
        private string[] get_Numbers(string numbers)
        {
            var delimiterList = new List<string>(new string[] { ",", "\n", ";" });

            if (numbers.Contains("//"))
            {
                numbers = string.Concat(numbers.Split('/'));
                delimiterList = get_Delimiters(numbers);
                numbers = string.Concat(numbers.Split('\n', '[', ']'));
            }

            return numbers.Split(delimiterList.ToArray(), StringSplitOptions.RemoveEmptyEntries);
        }
        private void validate_Delimiters(string[] numbArray)
        {
            var numberCharArray = string.Concat(numbArray).ToCharArray();
            var invalidDelimiters = string.Empty;

            foreach(var number in numberCharArray)
            {
                if (!int.TryParse(number.ToString(),out int validNumber) && !number.ToString().Contains("-"))
                {
                    invalidDelimiters = string.Join(" ", invalidDelimiters, number);
                }
            }
            if (!string.IsNullOrEmpty(invalidDelimiters))
            {
                throw new Exception("Invalid Delimiter !." + invalidDelimiters);
            }
        }

        private void validate_Numbers(string[] numberArray)
        {
            var negativeNumbers = string.Empty;

            foreach (var number in numberArray)
            {
                var wholeNumber = convertNumber(number);
                if (wholeNumber < 0)
                {
                    negativeNumbers = string.Join(" ", negativeNumbers, number);
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                throw new Exception("Negatives not Allowed." + negativeNumbers);
            }
        }

        private int convertNumber(string number)
        {
            return int.Parse(number);
        }

        private int get_Total(string[] numberArray)
        {
            var sum = 0;

            foreach(var number in numberArray)
            {
                var wholeNumber = convertNumber(number);
                if (wholeNumber <= 1000)
                {
                    sum += wholeNumber;
                }
            }

            return sum;
        }

    }
}
