﻿using System;
using NUnit.Framework;

namespace StringCalculatorV3
{
    [TestFixture]
    public class StringCalculatorTest
    {
        [Test]
        public void Given_Empty_String_When_Adding_Then_Return_Zero()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("");

            //Assert
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Given_One_Number_When_Adding_Then_GivenNumber()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("1");

            //Assert
            Assert.AreEqual(1, result);
        }

        [Test]
        public void Given_Two_Numbers_When_Adding_ThenReturn_Sum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("1,2");

            //Assert
            Assert.AreEqual(3, result);
        }

        [Test]
        public void Given_Any_Amount_Of_Numbers_When_Adding_Then_Return_Sum() 
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("10,1,1,1,2,5");

            //Assert
            Assert.AreEqual(20,result);
        }

        [Test]
        public void Given_New_Lines_Between_Numbers_When_Adding_Then_Return_Sum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("1\n2,3");

            //Assert
            Assert.AreEqual(6, result);
        }

        [Test]
        public void Given_Custom_Delimiters_When_Adding_Then_Return_Sum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//;\n1;2");

            //Assert
            Assert.AreEqual(3, result);
        }

        [Test]
        public void Given_Negative_Numbers_When_Adding_Then_Throw_Expcetion()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var exception = Assert.Throws<Exception>(()=>calculator.Add("//;\n2;-3;-4"));

            //Assert
            Assert.AreEqual("Negatives not Allowed. -3 -4",exception.Message);
        }

        [Test]
        public void Given_A_Number_Greater_Than_Thousand_When_Adding_Then_Return_Sum_Without_The_Number()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//;\n2;1001;1");

            //Assert
            Assert.AreEqual(3, result);
        }

        [Test]
        public void Given_Any_Length_Of_Delemiliters_When_Adding()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//***\n1***2***3");

            //Assert
            Assert.AreEqual(6, result);
        }
        
        [Test]
        public void Given_Multiple_Delimiters_When_Adding_Then_Return_Sum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//[*][%]\n1*2%3");

            //Assert
            Assert.AreEqual(6, result);
        }

        [Test]
        public void Given_Invalid_Delimiter_When_Adding_Then_Throw_Exception()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var exception = Assert.Throws<Exception>(() => calculator.Add("//[*][%]\n1*2$3"));

            //Assert
            Assert.AreEqual("Invalid Delimiter !. $", exception.Message);
        }

        [Test]
        public void Given_Any_Length_Of_Multiple_Delimiters_When_Adding_Then_Return_Sum()
        {
            //Arrange
            var calculator = new StringCalculator();

            //Act
            var result = calculator.Add("//[$$*][##%&]\n1$$*2##%&3");

            //Assert
            Assert.AreEqual(6,result);
        }
    }
}
